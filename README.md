### INTRODUCTION
Used so that the configuration created by Browser Development is able to be 
used by the CMS and Browser Development can be uninstalled.

#### REQUIREMENTS

Drupal 8 or 9.

#### INSTALLATION

Install using composer then using Drush or the Drupal UI enable the module.

#### CONFIGURATION

There is no configuration.
